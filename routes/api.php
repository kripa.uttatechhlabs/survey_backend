<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



/*
|--------------------------------------------------------------------------
| API Routes for Login and logout
|--------------------------------------------------------------------------
*/

Route::get('/login',[Auth\AuthController::class,'login'])->name('login'); // do not remove this

Route::group([
    //'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function($router) {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('logout', 'Auth\AuthController@logout');
    
});


Route::group([
    //'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'admin'
], function($router) {
    Route::post('plan/add', 'PlanController@store');
    Route::get('getAllPlan', 'PlanController@index');
    
});

Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'admin'
], function($comapny) {
    Route::post('company/add', 'CompanyRegistrationController@store');
    
    
    
});

Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'owner'
], function($router) {
    Route::post('subscription/add', 'SubscriptionController@store');
    Route::get('getAllSubscription', 'SubscriptionController@index');
    Route::post('publish', 'SubscriptionController@publish');
});

Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'client'
], function($router) {
    Route::post('register', 'ClientController@store');
});