<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\company_registration;

class CompanyRegistrationController extends Controller
{
    protected $user;
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->gourd()->user();
    }

    
    /**
     * add plan to the store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){
        //dd($request->file('fileKey'));

        //$request->file('fileKey')->store('feedback-survey/company-files','s3');
        //$img = storage_path('feedback-survey/company-files','p1.png');
        //dd($img);

        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'last_name'=>'required',
            'phone_number'=>'required',
            'address'=> 'required',

        ],[
            'first_name.required' => 'Please specify first name',
            'last_name.required' => 'Please specify last name',
            'phone_number.required' => 'Please specify phone number',
            'address.required' => 'Please specify address',
        ]);
        if($validator->fails()){
            $error_message = '<ul>';
            foreach($validator->messages()->getMessages() as $field_name => $messages) {
               foreach($messages AS $message) {
                    $error_message .= '<li>'.$message.'</li>';
                }
            }
            $error_message .= '</ul>';
            return response()->json(['status' => false, 'error' => $error_message], 200);
        }

        if(!$request->hasFile('official_file')){
            return response()->json(['status' => false, 'error'=>'Please specify company file'], 200);
        }

        if(!$request->hasFile('image') && $request->storeType == 'create'){
            return response()->json(['status' => false, 'error'=>'Please specify company image'], 200);
        }

        if($request->hasFile('official_file')){
            $official_file = $this->uploadFile($request->file('official_file'),'feedback-survey/company-files');
        }
        if($request->hasFile('image')){
            $company_logo = $this->uploadFile($request->file('image'),'feedback-survey/company-profile-images');
        }
        
        //Set new record for company
        $company = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'official_file' => $official_file,
            'image' => $company_logo
            
        );

        $newCompanyId= company_registration::create($company);
        
        // $newPlanID =$this->user->plans()->create($userPlan);

        return response()->json(['status' => true, 'message'=>'Company Created Successfully'], 200);
    }

    
    
    /**
     *  uploadFile to s3 bucket
     *
     * @param  mixed $upFile
     * @param  mixed $uploadPath
     * @return void
     */
    protected function uploadFile($upFile,$uploadPath){
        //Upload item Image
        $file = $upFile;
        $originalImage = $file->getClientOriginalName();
        $fileNameOnly = pathinfo($originalImage, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $newFile = str_replace(' ','_',$fileNameOnly).'_'.rand().'_'.time().'.'.$extension;
        $file->storeAs($uploadPath, $newFile,'s3');

        return $newFile;
    }


    protected function gourd(){
        return auth('api');
    }
}
