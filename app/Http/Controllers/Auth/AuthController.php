<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    
    
    /**
     * User login function validate emailand password
     *
     * @param  mixed $request
     * @return void
     */

    public function login(Request $request){
       
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:8'
        ]);

        if($validator->fails()){
            $error_message = '<ul>';
            foreach($validator->messages()->getMessages() as $field_name => $messages) {
                foreach($messages AS $message) {
                    $error_message .= '<li>'.$message.'</li>';
                }
            }

            $error_message .= '</ul>';
            return response()->json(['status' => false, 'error' => $error_message], 406);
        }

        if (!auth()->attempt(['email'=> $request->email,'password' => $request->password])) {
            return response()->json(['status' => false, 'error' => 'Unauthorized'], 200);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;

        //return response(['user' => auth()->user(), 'token' => $token]);
        return response()->json([
            'status' => true,
            'token' => $token,
            'token_type' => 'Bearer',
            'user_type'=> auth()->user()->user_type
        ], 200);
    }

    
    /**
     * logout the given user
     *
     * @return void
     */
    public function logout(){
        $user = auth()->user()->token();
        $user->revoke();
        return response()->json(['status' => true, 'message' => 'User logged out successfully'], 200);
    }
}
