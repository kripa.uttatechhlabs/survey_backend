<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Subscription;


class SubscriptionController extends Controller
{
    protected $user;
    
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->gourd()->user();
    }

    /**
     * Get the list of all subscription  
     *
     * @return void
     */
    public function index(){
       
       // $subscription = Subscription::where('published','Yes')->get();
       $subscription = Subscription::all();
        if($subscription->count()>0){
            return response()->json([
                'status' => true, 
                'records' => $subscription], 200);
        }
        else{
            return response()->json([
                'status' => false, 
                'error' => 'No subscription found'], 200);
        }

    }


     /**
     * add subscription to the storage
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'description'=>'required',
            'amount'=>'required',
            'free_count'=> 'required'
        ],[
            'title.required' => 'Please specify title',
            'description.required' => 'Please specify description',
            'amount.required' => 'Please specify amount',
            'free_count.required' => 'Please specify no of user',
        ]);
        if($validator->fails()){
            $error_message = '<ul>';
            foreach($validator->messages()->getMessages() as $field_name => $messages) {
               foreach($messages AS $message) {
                    $error_message .= '<li>'.$message.'</li>';
                }
            }
            $error_message .= '</ul>';
            return response()->json(['status' => false, 'error' => $error_message], 200);
        }
        
        $amount = ceil($request->amount);
        $free_count = $request->free_count;
        $amount_per_count = $amount/$free_count;

        //Set new record for Subscription
        $subscription = array(
            'title' => $request->title,
            'description' => $request->description,
            'amount' => $amount,
            'free_count' => $free_count,
            'amount_per_count' => ceil($amount_per_count)
            
        );
        
        $newSubscriptionID = Subscription::create($subscription);

        return response()->json(['status' => true, 'message'=>'Subscription Created Successfully'], 200);
    }

    
    /**
     * publish the subscription 
     *
     * @param  mixed $request
     * @return void
     */
    public function publish(Request $request){

        $subscription = array(
            'published' => 'Yes'
        );

        $newService = Subscription::where('id',$request->ID)->update($subscription);
        return response()->json(['status' => true, 'message'=>'Subscription published Successfully'], 200);
    }

    protected function gourd(){
        return auth('api');
    }

}
