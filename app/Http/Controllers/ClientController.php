<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Client_detail;
use App\Models\User_file;
use App\Models\Client_subscription_mapping;
use Illuminate\Support\Facades\DB;


class ClientController extends Controller
{
    protected $user;
    
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->gourd()->user();
    }

    
    /**
     * Add client to storage
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:users,email',
            //'password' => ''
            'phone'=>'required',
            'address'=> 'required',
            'subscription_id'=> 'required',
            'amount' => 'required',
            'free_count' => 'required'

        ],[
            'required.required' => 'Please specify first name',
            'last_name.required' => 'Please specify last name',
            'email.required' => 'Please specify email',
            'phone.required' => 'Please specify phone',
            'address.required' => 'Please specify address',
            'subscription_id.required' => 'Please specify subscription plan',
            'amount.required' => 'Please specify subscription amount',
            'free_count.required' => 'Please specify no of free user',
            
        ]);

        if($validator->fails()){
            $error_message = '<ul>';
            foreach($validator->messages()->getMessages() as $field_name => $messages) {
               foreach($messages AS $message) {
                    $error_message .= '<li>'.$message.'</li>';
                }
            }
            $error_message .= '</ul>';
            return response()->json(['status' => false, 'error' => $error_message], 200);
        }

        if(!$request->hasFile('profile_image')){
            return response()->json(['status' => false, 'error'=>'Please specify profile image'], 200);
        }

        if(!$request->hasFile('files')){
            return response()->json(['status' => false, 'error'=>'Please specify client files'], 200);
        }

        if($request->hasFile('profile_image')){
            $profile_image = $this->uploadFile($request->file('profile_image'),'feedback-survey/company-profile-images');
        }


        DB::beginTransaction();
        try {
            $password= 'password';
            $user=array(
                'email' => $request->email,
                'password' => bcrypt($password),
                'user_type' => 'Client'
            );

            $newUserId = User::create($user);

            //dd($newUserId->id);

            $userClientDetails = array(
                'first_name'=> $request->first_name,
                'last_name' =>  $request->last_name,
                'phone' => $request->phone,
                'address' => $request->address,
                'profile_image'=> $profile_image,
                'user_id' => $newUserId->id
            );

            $newUserClientDetails = Client_detail::create($userClientDetails);
            $clientSubscriptionMapping =$this->addClientSubscriptionMapping($request,$newUserId->id);
            $userFiles = $this->addUserfiles($request->files,$newUserId->id);

            DB::commit();

            return response()->json(['status' => true, 'message'=>'Client Created Successfully'], 200);
        
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'message'=>'Unable to create client'], 200);
        }

        

        
    }

    
     /**
     * Store the client and purchased subscription information
     *
     * @param  mixed $request
     * @param  mixed $userid
     * @return void
     */
    protected function addClientSubscriptionMapping($request,$userId){

        $amount = ceil($request->amount);
        $free_count = $request->free_count;
        $amount_per_count = $amount/$free_count;

        $subscriptionMapping =array(
            'user_id' => $userId,
            'subscription_id' => $request->subscription_id,
            'amount' => $amount,
            'free_count' => $free_count,
            'amount_per_count'=> $amount_per_count
        );

        //print_r($subscriptionMapping); die();
       // DB::enableQueryLog();
        Client_subscription_mapping::create($subscriptionMapping);
        //dd(DB::getQueryLog());
    }

    
    /**
     * Store the list of files supplied while creating client information
     *
     * @param  mixed $files
     * @param  mixed $userId
     * @return void
     */
    protected function addUserfiles($files, $userId){

        
        $userFilearray =[];
        foreach($files as $index => $file){
           
            $userFilearray[] = array(
                'user_id' => $userId,
                //'file' =>  $this->uploadFile($files[$index],'feedback-survey/company-files'),
                'file' =>  'ffgfgf',
                'file_status' => 'Yes'
            );
        }
        //print_r($userFilearray); die();
        User_file::insert($userFilearray);

    } 



     /**
     *  uploadFile to s3 bucket
     *
     * @param  mixed $upFile
     * @param  mixed $uploadPath
     * @return void
     */
    protected function uploadFile($upFile,$uploadPath){
        //Upload item Image
        $file = $upFile;
        $originalImage = $file->getClientOriginalName();
        $fileNameOnly = pathinfo($originalImage, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $newFile = str_replace(' ','_',$fileNameOnly).'_'.rand().'_'.time().'.'.$extension;
        $file->storeAs($uploadPath, $newFile,'s3');

        return $newFile;
    }


    protected function sendEmail(){

    }

    protected function gourd(){
        return auth('api');
    }
}
