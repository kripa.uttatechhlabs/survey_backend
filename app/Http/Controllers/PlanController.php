<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Plan;

class PlanController extends Controller
{
    protected $user;
    //
    public function __construct()
    {
        //$this->middleware('auth:api');
        ///$this->user = $this->gourd()->user();
    }

    
    /**
     * Get the list of all plan  
     *
     * @return void
     */
    public function index(){
        $password = bcrypt('password');
        dd($password);
        $plan = Plan::all();
        if($plan->count()>0){
            return response()->json([
                'status' => true, 
                'records' => $plan], 200);
        }
        else{
            return response()->json([
                'status' => false, 
                'error' => 'No Plans found'], 200);
        }

    }

    
    /**
     * add plan to the store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'description'=>'required',
            'price'=>'required',
            'total_no_user'=> 'required'
        ],[
            'title.required' => 'Please specify title',
            'description.required' => 'Please specify description',
            'price.required' => 'Please specify price',
            'total_no_user.required' => 'Please specify no of user',
        ]);
        if($validator->fails()){
            $error_message = '<ul>';
            foreach($validator->messages()->getMessages() as $field_name => $messages) {
               foreach($messages AS $message) {
                    $error_message .= '<li>'.$message.'</li>';
                }
            }
            $error_message .= '</ul>';
            return response()->json(['status' => false, 'error' => $error_message], 200);
        }
        
        //Set new record for Plan
        $userPlan = array(
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'total_no_user' => $request->total_no_user,
            
        );
        
        $newPlanID =$this->user->plans()->create($userPlan);

        return response()->json(['status' => true, 'message'=>'Plan Created Successfully'], 200);
    }


    protected function gourd(){
        return auth('api');
    }

}
