<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client_subscription_mapping extends Model
{
    use HasFactory;
    
    /**
     * fillable 
     *
     * @var array
     */

    protected $fillable = [
        // 'first_name',
        // 'last_name',
        // 'phone',
        // 'address',
        // 'profile_image',
        'user_id',
        'subscription_id',
        'amount',
        'free_count',
        'amount_per_count'
    ];

  
    /**
     * hidden
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}
