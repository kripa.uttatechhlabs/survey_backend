<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory; 

    /**
     * fillable 
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'amount',
        'free_count',
        'amount_per_count',
        'published'
    ];

  
    /**
     * hidden
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];




}
