<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;
    
    
    /**
     * fillable 
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'total_no_user',
        'user_id'
    ];

  
    /**
     * hidden
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function Users(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
}
