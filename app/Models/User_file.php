<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_file extends Model
{
    use HasFactory;

    /**
     * fillable 
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'file',
        'file_status',
    ];

  
    /**
     * hidden
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}
